#!/bin/sh

. /app/config/app.conf

curl ${CURL_SRCOPTS} -H "Authorization: Bearer ${BEARERTOKEN}" "${SRC}" | xsltproc ${XSLTPROC_OPTS} ${XSLTPROC_STYLESHEET} - | curl ${CURL_DESTOPTS} -u "${DESTUSER}:${DESTPASS}" -T - "${DEST}"
