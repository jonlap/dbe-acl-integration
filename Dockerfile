FROM alpine:3.9
RUN apk add --no-cache libxslt curl

RUN addgroup -g 1000 app
RUN adduser -u 1000 -G app -s /bin/sh -D app

WORKDIR /app
COPY app.sh /app/
RUN chown -R app:app /app
USER app  

ENTRYPOINT ["/app/app.sh"]
